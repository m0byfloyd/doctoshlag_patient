class Appointment {
  Appointment({
    required this.doctor,
    required this.patient,
    required this.datetime,
    this.reason
  });

  final DateTime datetime;
  final String doctor;
  final String patient;
  String? reason;

  factory Appointment.fromJson(Map<String, dynamic> json) {
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(
        json['datetime'].seconds * 1000 + json['datetime'].nanoseconds ~/ 1000000);

    return Appointment(
      patient: json['patient'],
      datetime: dateTime,
      doctor: json['doctor'],
      reason:json['reason']
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'patient': patient,
      'doctor': doctor,
      'date': datetime,
      'reason':reason
    };
  }
}
