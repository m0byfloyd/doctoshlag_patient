import 'package:firebase_auth/firebase_auth.dart'
    hide EmailAuthProvider, PhoneAuthProvider;

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../app_state.dart';
import '../components/menu.dart';
import 'authentication.dart';
import 'doctor_list.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Accueil')),
      drawer: const Menu(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Consumer<ApplicationState>(
            builder: (context, appState, _) => Column(
              children: [
                Visibility(
                  visible: appState.loggedIn,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text('Liste des médecins'),
                      DoctorList(doctors: appState.doctors),
                      //DoctorList(doctors: [Doctor(name: 'pierre')]),
                    ],
                  ),
                ),
                Visibility(
                    visible: !appState.loggedIn,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text('Veuillez vous connecter pour poursuivre'),
                        AuthFunc(
                            loggedIn: appState.loggedIn,
                            signOut: () {
                              FirebaseAuth.instance.signOut();
                            }),
                      ],
                    )
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
