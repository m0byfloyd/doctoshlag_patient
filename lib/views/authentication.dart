import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class AuthFunc extends StatelessWidget {
  const AuthFunc({
    super.key,
    required this.loggedIn,
    required this.signOut,
  });

  final bool loggedIn;
  final void Function() signOut;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Visibility(
            visible: !loggedIn,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                TextButton(
                    onPressed: () {
                      context.push('/sign-in');
                    },
                    child: const Text('Connexion')),
              ],
            )),
        Visibility(
            visible: loggedIn,
            child: Column(children: [
              Padding(
                padding: const EdgeInsets.only(left: 24, bottom: 8),
                child: TextButton(
                    onPressed: () {
                      context.push('/profile');
                    },
                    child: const Text('Profile')),
              ),
              TextButton(
                  onPressed: () {
                    signOut();
                  },
                  child: const Text('J\'veux en finir')),
            ])
        ),
      ],
    );
  }
}