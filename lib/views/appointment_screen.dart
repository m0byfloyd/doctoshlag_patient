import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:doctorshlagpatient/app_state.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AppointmentScreen extends StatefulWidget {
  const AppointmentScreen({super.key});

  @override
  _AppointmentScreenState createState() => _AppointmentScreenState();
}

class _AppointmentScreenState extends State<AppointmentScreen> {
  DateTime? _dateTime;
  String? _reason;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Prendre un rendez-vous'),
        ),
        body: Consumer<ApplicationState>(
          builder: (context, appState, _) => Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('Vous prenez un rendez vous avec le docteur ${appState.selectedDoctor?.name}'),
              ElevatedButton(
                child: const Text('Choisir une date et une heure'),
                onPressed: () async {
                  final DateTime? selectedDateTime = await showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime.now(),
                    lastDate: DateTime(2100),
                  );

                  if (selectedDateTime != null) {
                    final TimeOfDay? selectedTime = await showTimePicker(
                      context: context,
                      initialTime: TimeOfDay.fromDateTime(DateTime.now()),
                    );

                    if (selectedTime != null) {
                      setState(() {
                        _dateTime = DateTime(
                          selectedDateTime.year,
                          selectedDateTime.month,
                          selectedDateTime.day,
                          selectedTime.hour,
                          selectedTime.minute,
                        );
                      });
                    }
                  }
                },
              ),
              TextField(
                decoration: const InputDecoration(
                  labelText: 'Raison de votre rdv',
                ),
                onChanged: (value) {
                  setState(() {
                    _reason = value;
                  });
                },
              ),
              ElevatedButton(
                child: const Text('Enregistrer le rendez-vous'),
                //onPressed: () {
                onPressed: () async {
                  if (_dateTime == null) {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: const Text('Erreur'),
                          content:
                              const Text('Merci de sélectionner une heure de rdv'),
                          actions: [
                            TextButton(
                              child: const Text('OK'),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  }

                  print(_reason);

                  if (_reason == null) {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: const Text('Erreur'),
                          content:
                          const Text('Merci d\'indiquer la raison de votre venue'),
                          actions: [
                            TextButton(
                              child: const Text('OK'),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  }


                  if (_dateTime != null && _reason != null) {
                    await FirebaseFirestore.instance
                        .collection('appointment')
                        .add({
                      'doctor': appState.selectedDoctor?.name,
                      'dateTime': _dateTime,
                      'patient': FirebaseAuth.instance.currentUser?.displayName,
                      'reason':_reason
                    });

                    if (!context.mounted) return;

                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text('Succès'),
                          content: Text('Votre rendez-vous a été enregistré.'),
                          actions: [
                            TextButton(
                              child: const Text('OK'),
                              onPressed: () {
                                Navigator.of(context).pop();
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  }
                },
              ),
            ],
          ),
        ));
  }
}
