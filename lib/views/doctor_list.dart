import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../app_state.dart';
import '../models/doctor.dart';

class DoctorList extends StatefulWidget {
  const DoctorList({
    super.key,
    required this.doctors,
  });

  final List<Doctor> doctors;

  @override
  State<DoctorList> createState() => _DoctorList();
}

class _DoctorList extends State<DoctorList> {
  @override
  Widget build(BuildContext context) {
    return Consumer<ApplicationState>(
        builder: (context, state, _) => Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            for (final doctor in widget.doctors)
              Card(
                child: Column(
                  children: [
                    ListTile(
                      leading: const Icon(Icons.person),
                      title: Text(doctor.name),
                    ),

                    ElevatedButton(
                        onPressed: () {
                          context.read<ApplicationState>()
                              .setSelectedDoctor(doctor);

                          context.read<ApplicationState>()
                              .getSelectedDoctorAppointments();

                          context.push('/appointment');
                        },
                        child: Row(
                          children: const [
                            Icon(Icons.date_range),
                            Text('Prendre rendez-vous avec ce praticien')
                          ],
                        ))
                  ],
                ),
              ),
          ],
        ));
  }
}
