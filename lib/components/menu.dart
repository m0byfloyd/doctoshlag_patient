import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../app_state.dart';

class Menu extends StatefulWidget {
  const Menu({super.key});

  @override
  State<Menu> createState() => _Menu();
}

class _Menu extends State<Menu> {
  var items = [];

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text('Doctoshlag'),
          ),
          ListTile(
            title: const Text('Accueil'),
            onTap: () {
              Navigator.pop(context);
              context.push('/');
            },
          ),
          if (context.watch<ApplicationState>().loggedIn)
            ListTile(
              title: const Text('Profil'),
              onTap: () {
                Navigator.pop(context);
                context.push('/profile');
              },
            ),
          if (!context.watch<ApplicationState>().loggedIn)
            ListTile(
              title: const Text('Se connecter'),
              onTap: () {
                Navigator.pop(context);
                context.push('/sign-in');
              },
            ),
        ],
      ),
    );
  }
}
