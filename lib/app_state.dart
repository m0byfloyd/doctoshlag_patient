import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart'
    hide EmailAuthProvider, PhoneAuthProvider;
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_ui_auth/firebase_ui_auth.dart';
import 'package:flutter/material.dart';

import 'models/appointment.dart';
import 'models/doctor.dart';
import 'firebase_options.dart';

class ApplicationState extends ChangeNotifier {
  ApplicationState() {
    init();
  }

  bool _loggedIn = false;

  bool get loggedIn => _loggedIn;

  StreamSubscription<QuerySnapshot>? _doctorsSubscription;
  List<Doctor> _doctors = [];

  List<Doctor> get doctors => _doctors;

  void setSelectedDoctor(Doctor selectedDoctor) {
    _selectedDoctor = selectedDoctor;
    notifyListeners();
  }

  StreamSubscription<QuerySnapshot>? _selectedDoctorSubscription;
  Doctor? _selectedDoctor;

  Doctor? get selectedDoctor => _selectedDoctor;

  StreamSubscription<QuerySnapshot>? _appointmentsSubscription;
  List<Appointment> _appointments = [];

  List<Appointment> get appointments => _appointments;

  List<Appointment> _selectedDoctorAppointments = [];

  List<Appointment> get selectedDoctorAppointments =>
      _selectedDoctorAppointments;

  Future<void> getSelectedDoctorAppointments() async {
    final doctorAppointmentsQueryResults = await FirebaseFirestore.instance
        .collection('appointment')
        .where('doctor', isEqualTo: selectedDoctor!.name)
        .orderBy('datetime')
        .get();

    final slots =
    doctorAppointmentsQueryResults.docs.map((e) => e.data()).toList();
    List<Appointment> doctorAppointments = [];

    for (final slot in slots) {
      doctorAppointments.add(Appointment.fromJson(slot));
    }

    _selectedDoctorAppointments = doctorAppointments;

    notifyListeners();
  }

  Future<void> init() async {
    await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform);

    FirebaseUIAuth.configureProviders([
      EmailAuthProvider(),
    ]);

    FirebaseAuth.instance.userChanges().listen((user) {
      if (user != null) {
        _loggedIn = true;
        _doctorsSubscription = FirebaseFirestore.instance
            .collection('doctors')
            .snapshots()
            .listen((snapshot) {
          _doctors = [];
          for (final document in snapshot.docs) {
            _doctors.add(
              Doctor(
                name: document.data()['name'] as String,
              ),
            );
          }
          notifyListeners();
        });

        _appointmentsSubscription = FirebaseFirestore.instance
            .collection('appointments')
            .snapshots()
            .listen((snapshot) {
          _appointments = [];
          for (final document in snapshot.docs) {
            _appointments.add(
              Appointment(
                  doctor: document.data()['doctor'] as String,
                  patient: document.data()['patient'] as String,
                  datetime: (document.data()['datetime'] as Timestamp).toDate(),
                  reason:  document.data()['reason'] as String
              ),
            );
          }
          notifyListeners();
        });
      } else {
        _loggedIn = false;
      }
      notifyListeners();
    });
  }
}
